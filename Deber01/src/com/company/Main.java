package com.company;

public class Main {
    //Podemos acceder a el mediante el nombre de la clase
    public static void main(String[] args) {

        Mates Mates = new Mates();
        Constantes Constantes = new Constantes();
        System.out.println(Mates.pi);
        System.out.println("PI = " + Constantes.PI);
        System.out.println("E = " + Constantes.E);
    }
}
     class Mates {
        //Atributo de la clase Mates static
        public static float pi = 3.14;

    }

     class Constantes {
        //Constantes publicas
        public static final float PI = 3.141592f;
        public static final float E = 2.728281f;


    }

    abstract class Figura {

        // Variable que contendra el nombre de la figura
        private String name;

        // setter para definir el nombre de la figura
        public void setName(String name) {
            this.name=name;
        }

        // getter para obtener el nombre de la figura
        public String getName() {
            return this.name;
        }

        // Definiemos una clase abstracta
        // Creas un patron de diseño que tienes que seguir obligatoriamente
        // en tu jerarquia de herencia
        public abstract String tipoFigura();

    }










class Rectangulo extends Figura {

    public Rectangulo(String name) {
        super.setName(name);
    }

    // Estamos obligados a crear este metodo, ya que heredamos de Animal
    // que es una clase abstracta, por lo que hay que definir todos los
    // metodos abstractos que tenga
    public String tipoFigura() {
        return "El La figura es un  " + super.getName();
    }
}
